# Code Challenge - Digital Republic

Conforme a tarefa enviada, a responsabilidade era de criar uma calculadora que fizesse os cálculos das quantidade de tinta que seria utilizada para pintar um ambiente com tamanhos que seriam fornecidos pelo usuário.
O app conta com as validações necessárias para executar o projeto, bem como com uma área onde são relacionados os ambientes que já foram calculados(extra).
Tentei utilizar ao máximo o JS e React, sem a necessidade de instalar outras ferramentas para executar as ações. Foi um pouco difícil, até deu mais trabalho.

## Instalação

O projeto foi realizado utilizando ReactJS. Sendo assim, após clonar o repositório, navegue até a pasta ink-calculator e instale as dependências do projeto.

```bash
npm install 
```

## Utilização
Para rodar o projeto, basta digitar o comando abaixo e esperar que o seu navegador padrão se abra com o projeto em funcionamento.

```bash
npm start
```

## Agradecimentos e considerações
Quero agradecer pelo contato e pela oportunidade de participar do Code Challenge. Realizei o trabalho com muita dedicação e atenção. Algum coisa ou outra poderia ser mudada, sim. Porém devido ao pouco tempo, outros trabalhos e outras demandas, talvez não tenha ficado tão bom quanto poderia ficar e como idealizei.
Levem em consideração que o projeto foi realizado sozinho...temos que concordar que programar em equipe e realizar trabalhos desse tipo compartilhando ideias e informações é muito melhor.

Meu muito obrigado e vou ficar torcendo daqui. Gostei muito da empresa!!!