import styled from "styled-components";

export const Container = styled.div`
  height: m;
  background: #f6921e;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px;
`;

export const Title = styled.h1`
  color: #ffffff;
  font-size: 3em;
  font-weight: bold;
`;

export const ButtonStar = styled.button`
  background-color: ${(props) => (props.disabled ? "#CCC" : "#FFF")};
  color: ${(props) => (props.disabled ? "#AAA" : "#f6921e")};
  font-size: 2.2em;
  font-weight: bold;
  border: none;
  padding: 15px 40px;
  border-radius: 20px;
  margin: ${(props) => (props.margin ? props.margin : "0 0 10px 0")};
`;
