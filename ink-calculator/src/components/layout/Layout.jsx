import React from "react";
import { Home } from "../home/Home";
import { Container } from "./styles/styles";

export const Layout = () => {
  return (
    <Container>
      <Home />
    </Container>
  );
};
