import styled from "styled-components";




export const Title = styled.h1`
color:#FFFFFF;
font-size: ${ props => props.fontSize ? props.fontSize : '3em'  };
font-weight:bold;

`;

export const InputNameSpace = styled.input `
width: 75%;
margin-bottom:20px;
border: none;
border-bottom: 2px solid #FFF;
color: #FFF;
padding:20px;
font-size: 1.5em;
background: none;

::placeholder {
    font-weight: bold;
    opacity: 0.5;
    color: #FFF;
}

`;