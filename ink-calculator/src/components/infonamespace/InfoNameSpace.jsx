import React, { useState } from "react";

import { InputNameSpace, Title } from "./styles/styles";
import { StartButton } from "../home/styles/styles";


export const InfoNameSpace = (props) => {
  

  const handleClick = () => {
    props.onClickCalc();
    props.onClickRegisterNamePlace(namePlace);
    setShowHome(false);
    setShowFirstStep(true);
  };

  const [showHome, setShowHome] = useState(true);
  const [showFirstStep, setShowFirstStep] = useState(false);
  const [namePlace, setNamePlace] = useState("");

  return (
    <>
      <Title>
        Insira o nome do espaço para o qual deseja realizar o cálculo
      </Title>
      <InputNameSpace
        placeholder="Nome do espaço"
        type={"text"}
        onChange={(e) => setNamePlace(e.target.value)}
      ></InputNameSpace>
      <StartButton onClick={handleClick}>Calcular</StartButton>
    </>
  );
};
