import styled from "styled-components";

export const Title = styled.h1`
  color: #ffffff;
  font-size: ${(props) => (props.fontSize ? props.fontSize : "3em")};
  font-weight: bold;
`;

export const StartButton = styled.button`
  background-color: #fff;
  color: #f6921e;
  font-size: ${(props) => (props.fontSize ? props.fontSize : "2.2em")};
  font-weight: bold;
  border: none;
  padding: 15px 40px;
  border-radius: 20px;
  margin-bottom: 10px;
`;
