import React, { useState, useEffect } from "react";
import { InfoWall } from "../infowall/InfoWall";
import { StartButton, Title } from "./styles/styles";
import { InfoNameSpace } from "../infonamespace/InfoNameSpace";
import { Places } from "../places/Places";

export const Home = () => {
  useEffect(() => {
    const data = localStorage.getItem("myPlaces");
    setPlaces(JSON.parse(data));
  }, []);

  const handleClickNext = (action) => {
    if (action === "calc") {
      setShowNameSpace(false);
      setShowHome(false);
      setShowFirstStep(true);
    } else if (action === "infoname") {
      setShowHome(false);
      setShowNameSpace(true);
    } else if (action === "restart") {
      setShowFirstStep(false);
      setShowNameSpace(true);
      setPlace("");
    } else if (action === "meusespacos") {
      setShowHome(false);
      setShowMySpaces(true);
    }
  };

  const registerNamePlace = (name) => {
    setPlace(name);
  };

  const [showHome, setShowHome] = useState(true);
  const [showNameSpace, setShowNameSpace] = useState("");
  const [showFirstStep, setShowFirstStep] = useState(false);
  const [showMySpaces, setShowMySpaces] = useState(false);
  const [place, setPlace] = useState("");
  const [places, setPlaces] = useState([]);

  return (
    <>
      {showHome ? (
        <>
          <Title>
            Vamos calcular a quantidade de tinta que você precisará para pintar
            o seu espaço?
          </Title>

          <StartButton onClick={() => handleClickNext("infoname")}>
            Vamos lá!
          </StartButton>
          {places[0] ? (
            <StartButton
              fontSize={"1.5em"}
              onClick={() => handleClickNext("meusespacos")}
            >
              Meus espaços
            </StartButton>
          ) : null}
        </>
      ) : (
        false
      )}

      {showFirstStep ? (
        <InfoWall
          nameSpace={place}
          onButtonClickRestart={() => handleClickNext("restart")}
        />
      ) : (
        false
      )}

      {showMySpaces ? <Places /> : null}

      {showNameSpace ? (
        <>
          <InfoNameSpace
            onClickCalc={() => handleClickNext("calc")}
            onClickRegisterNamePlace={registerNamePlace}
          />
        </>
      ) : (
        false
      )}
    </>
  );
};
