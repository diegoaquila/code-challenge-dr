import React, { useEffect, useState } from "react";
import {
  BlockItem,
  Container,
  ItemTitle,
  ItemSubTitle,
  ButtonDelete,
} from "./styles/styles";
import { calcInkCans } from "../../helpers/calcInkCans";
import { calcAreaTotalDoorWindow } from "../../helpers/calcAreaTotalDoorWindow";

export const Places = () => {
  const [places, setPlaces] = useState([]);

  useEffect(() => {
    const data = localStorage.getItem("myPlaces");
    setPlaces(JSON.parse(data));
  }, []);

  const deletePlace = (index) => {
    const placesFiltered = places.filter((e, eindex) => {
      return eindex !== index;
    });

    localStorage.setItem("myPlaces", JSON.stringify(placesFiltered));
    setPlaces(placesFiltered);
  };

  return (
    <>
      <Container>
        {places
          .map((e, index) => (
            <BlockItem key={index}>
              <ItemTitle>{e.nameSpace}</ItemTitle>
              <ItemSubTitle>Área Total: {e.wallArea}m&sup2;</ItemSubTitle>
              <ItemSubTitle>Portas: {e.qtDoor}</ItemSubTitle>
              <ItemSubTitle>Janelas: {e.qtWindow}</ItemSubTitle>
              <ItemSubTitle>Você irá precisar de:</ItemSubTitle>
              <ItemSubTitle>
                {calcInkCans(
                  ((e.wallArea - calcAreaTotalDoorWindow(e.qtDoor, e.qtWindow))/5)
                    
                )}
              </ItemSubTitle>
              <ButtonDelete onClick={() => deletePlace(index)}>
                Excluir espaço
              </ButtonDelete>
            </BlockItem>
          ))
          .reverse()}
      </Container>
    </>
  );
};
