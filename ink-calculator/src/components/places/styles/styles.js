import styled from 'styled-components';

export const TitlePlaces = styled.h1 `
font-size:2.5em;
color: #FFF;
text-align:left;
margin: 0;
`;
export const SubTitleWall = styled.h2 `
font-size:1.5em;
color: #FFF;
text-align:left;
margin: 0;
`;

export const Container = styled.div `
display: flex;
flex-direction: column;

`;

export const BlockItem = styled.div `
display: flex;
flex-direction: column;
background: none;
border-radius: 5px;
border: 2px solid #FFF;
padding: 10px;
margin-bottom:20px;
gap:5px;

`;

export const ItemTitle = styled.h2`
color: #FFF;
margin:0px 0 10px 0 ;
`;
export const ItemSubTitle = styled.h3`
color: #FFF;
font-size:1em;
margin: 0;
`;




export const ButtonDelete = styled.button`
background-color:${props => props.disabled? '#CCC' : '#FFF'} ;
color:${props => props.disabled? '#AAA' : '#f6921e'};
font-size:1em;
font-weight:bold;
border:none;
padding: 15px 40px;
border-radius:10px ;

`;

export const InputMeasures = styled.input `
width: 75%;
margin-bottom:20px;
border: none;
border-bottom: 2px solid #FFF;
color: #FFF;
padding:20px;
font-size: 1.5em;
background: none;

::placeholder {
    font-weight: bold;
    opacity: 0.5;
    color: #FFF;
} `;

export const LabelInputs = styled.label `
font-size:1em;
color: #FFF;
font-weight:bold;
`;