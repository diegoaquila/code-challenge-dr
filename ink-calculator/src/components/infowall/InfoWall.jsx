import React, { useState, useEffect } from "react";
import { ButtonStar } from "../layout/styles/styles";
import { verifyAreaWall } from "../../helpers/verifyAreaWall";
import { verifyAreaDoorAndWindow } from "../../helpers/verifyAreaDoorAndWindow";
import {
  getLocalStorage,
  setLocalStorage,
} from "../../services/useLocalStorage";
import { clearInputs } from "../../helpers/clearInputs";
import { configArray } from "../../helpers/configArrayLocalStorage";

import {
  AlertTitle,
  Container,
  InputMeasures,
  InputRadio,
  LabelInputs,
  SubTitleWall,
  TitleWall,
} from "./styles/styles";
import { Places } from "../places/Places";

export const InfoWall = (props) => {
  const [width, setWidth] = useState("");
  const [height, setHeight] = useState("");
  const [areaWall, setAreaWall] = useState(0);
  const [textAreaWall, setTextAreaWall] = useState("");
  const [textExceededArea, setTextExceededArea] = useState("");

  const [ButtonState, setButtonState] = useState(true);
  const [door, setDoor] = useState(false);
  const [textDoor, setTextDoor] = useState("");

  const [window, setWindow] = useState(false);
  const [qtDoor, setQtdoor] = useState(0);
  const [qtWindow, setQtWindow] = useState(0);
  const [indexWall, setIndexWall] = useState(0);
  const [walls, setWalls] = useState([]);

  const restart = () => {
    setIndexWall(0);
    setWalls([]);

    props.onButtonClickRestart();
  };

  const clear = () => {
    clearInputs(
      setWidth,
      setHeight,
      setDoor,
      setQtdoor,
      setWindow,
      setQtWindow,
      setAreaWall,
      setButtonState
    );
  };

  const handleClick = () => {
    setWalls([
      ...walls,
      {
        nameSpace: props.nameSpace,
        numberWall: Number(indexWall + 1),
        width: Number(width),
        height: Number(height),
        wallArea: Number((width * height).toFixed(2)),
        door: Boolean(door),
        window: Boolean(window),
        qtDoor: Number(qtDoor),
        qtWindow: Number(qtWindow),
      },
    ]);
    setIndexWall(indexWall + 1);
    clearInputs(
      setWidth,
      setHeight,
      setDoor,
      setQtdoor,
      setWindow,
      setQtWindow,
      setAreaWall,
      setButtonState
    );
  };

  useEffect(() => {
    if (walls) {
      setWalls(walls);
    }

    if (areaWall) {
      verifyAreaWall(setButtonState, areaWall, setTextAreaWall);
    }

    if (width !== "" || height !== "") {
      setAreaWall(width * height);
    }

    if (qtDoor || qtWindow) {
      verifyAreaDoorAndWindow(
        setButtonState,
        areaWall,
        qtDoor,
        qtWindow,
        setTextExceededArea
      );
    }

    if (width === "" || height === "") {
      setButtonState(true);
    }

    if (indexWall) {
      if (indexWall >= 4) {
        setLocalStorage("myPlaces", configArray(walls));
      }
    }
  }, [walls, areaWall, width, height, qtDoor, qtWindow, indexWall]);

  useEffect(() => {
    const getPlaces = getLocalStorage("myPlaces");

    if (!getPlaces) {
      setLocalStorage("myPlaces", []);
    }
  }, []);

  return (
    <>
      <Container>
        {indexWall >= 4 ? (
          <Places></Places>
        ) : (
          <>
            <TitleWall>Parede {indexWall + 1}</TitleWall>
            <SubTitleWall>Área: {areaWall.toFixed(2)}mts</SubTitleWall>
            {textAreaWall ? <AlertTitle>{textAreaWall}</AlertTitle> : null}

            <LabelInputs>
              Largura
              <InputMeasures
                min={0}
                placeholder="0.00"
                type="number"
                name="width"
                value={width}
                onChange={(e) => setWidth(e.target.value)}
              />
            </LabelInputs>

            <LabelInputs>
              Altura
              <InputMeasures
                min={0}
                placeholder="0.00"
                type="number"
                name="height"
                value={height}
                onChange={(e) => setHeight(e.target.value)}
              />
            </LabelInputs>

            <p>{textDoor}</p>

            {textExceededArea ? (
              <AlertTitle>{textExceededArea}</AlertTitle>
            ) : null}

            <LabelInputs>
              A parede possui porta?
              <InputRadio
                disabled={height >= 2.2 ? false : true}
                type="radio"
                name="door"
                checked={door === true}
                onChange={() => setDoor(true)}
              />
              {height < 2.2 && height ? (
                <AlertTitle>
                  Altura mínima da parede com porta: 2.20mts
                </AlertTitle>
              ) : null}
              {height >= 2.2 && door ? (
                <>
                  Informe a quantidade de portas
                  <InputMeasures
                    min={0}
                    type="number"
                    name="door"
                    value={qtDoor}
                    onChange={(e) => setQtdoor(e.target.value)}
                  />
                </>
              ) : null}
            </LabelInputs>
            <LabelInputs>
              A parede possui Janela?
              <InputRadio
                disabled={width >= 2 ? false : true}
                type="radio"
                name="window"
                checked={window === true}
                onChange={() => setWindow(true)}
              />
              {width >= 2 && window ? (
                <>
                  Informe a quantidade de Janelas
                  <InputMeasures
                    min={0}
                    type="number"
                    name="window"
                    value={qtWindow}
                    onChange={(e) => setQtWindow(e.target.value)}
                  />
                </>
              ) : (
                false
              )}
            </LabelInputs>
          </>
        )}

        {indexWall >= 4 ? (
          <>
            <ButtonStar onClick={restart}>Recalcular</ButtonStar>
          </>
        ) : (
          <>
            <ButtonStar
              margin={"30px 0 10px 0"}
              disabled={ButtonState}
              onClick={handleClick}
            >
              Confirmar
            </ButtonStar>
            <ButtonStar onClick={clear}>Limpar</ButtonStar>
            <ButtonStar onClick={restart}>Reiniciar</ButtonStar>
          </>
        )}
      </Container>
    </>
  );
};
