import styled from "styled-components";

export const TitleWall = styled.h1`
  font-size: 2.5em;
  color: #fff;
  text-align: left;
  margin: 0;
`;
export const SubTitleWall = styled.h2`
  font-size: 1.5em;
  color: #fff;
  text-align: left;
  margin: 0;
`;
export const AlertTitle = styled.p`
  font-size: 1em;
  color: #fff;
  text-align: left;
  background: red;
  padding: 10px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Button = styled.button`
  background: ${(props) => (props.disabled ? "blue" : "#FFF")};
  padding: 15px 20px;
  color: #f6921e;
  font-size: 2.2em;
`;

export const InputMeasures = styled.input`
  width: 75%;
  margin-bottom: 20px;
  border: none;
  border-bottom: 2px solid #fff;
  color: #fff;
  padding: 20px;
  font-size: 1.5em;
  background: none;

  ::placeholder {
    font-weight: bold;
    opacity: 0.5;
    color: #fff;
  }
`;

export const InputRadio = styled.input`
  border: 0px;
  width: 25px;
  height: 30px;
  vertical-align: middle;
`;

export const LabelInputs = styled.label`
  font-size: 1em;
  color: #fff;
  font-weight: bold;
`;
