export const calcInkCans = (value) => {
  const cans = [18, 3.6, 2.5, 0.5];
  let usedCans = [];

  for (let can of cans) {
    const quantity = Math.floor(value / can);
    value %= can;

    if (quantity > 0) {
      usedCans.push(`${quantity} lata(s) de ${can}L`);
    }
  }
  return usedCans.join(", ");
};
