import { standardMeasures } from "../utils/standardMeasures";

export const calcAreaTotalDoorWindow = (qtdoor, qtwindow) => {
  const totalAreaDoor = qtdoor * standardMeasures.door.doorArea;
  const totalAreaWindow = qtwindow * standardMeasures.window.windowArea;

  return totalAreaDoor + totalAreaWindow;
};
