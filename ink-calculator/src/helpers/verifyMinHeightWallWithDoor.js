import { standardMeasures } from "../utils/standardMeasures";

export const verifyMinHeightWall = (
  height,
  setTextDoor,
  stateButton,
  setButtonState,
  qtDoor,
  textMinHeightWall,
  setTextMinHeightWall
) => {
  if (qtDoor > 0 && height < standardMeasures.minHeightWallWithDoor) {
    setTextDoor(
      `A parede com porta deve ter uma altura mínima de ${standardMeasures.minHeightWallWithDoor}mts`
    );
    stateButton = true;
    setButtonState(stateButton);
  } else {
    setTextDoor("");
    stateButton = false;
    setButtonState(stateButton);
  }
};
