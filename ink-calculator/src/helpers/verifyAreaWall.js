export const verifyAreaWall = (setButtonState, areaWall, setTextAreaWall) => {
  if (areaWall > 50 || areaWall < 1) {
    setButtonState(true);
    if (areaWall > 50 && areaWall !== 0) {
      setTextAreaWall("A área da parede deve ser menor que 50m2");
    } else if (areaWall < 1 && areaWall !== 0) {
      setTextAreaWall("A área da parede deve ser maior que 1m2");
    } else {
      setTextAreaWall("");
    }
  } else {
    setButtonState(false);
    setTextAreaWall("");
  }
};
