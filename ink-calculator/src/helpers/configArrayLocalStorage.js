export const configArray = (Array) => {
  return Array.reduce((acc, wall) => {
    return {
      nameSpace: wall.nameSpace,
      wallArea: acc.wallArea + wall.wallArea,
      qtDoor: acc.qtDoor + wall.qtDoor,
      qtWindow: acc.qtWindow + wall.qtWindow,
    };
  });
};
