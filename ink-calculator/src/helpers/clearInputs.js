export const clearInputs = (
  setWidth,
  setHeight,
  setDoor,
  setQtdoor,
  setWindow,
  setQtWindow,
  setAreaWall,
  setButtonState
) => {
  const newValues = {
    width: "",
    height: "",
    door: false,
    qtdoor: 0,
    window: false,
    qtWindow: 0,
    areaWall: 0,
    ButtonState: true,
  };

  setWidth(newValues.width);
  setHeight(newValues.height);
  setDoor(newValues.door);
  setQtdoor(newValues.qtdoor);
  setWindow(newValues.window);
  setQtWindow(newValues.qtWindow);
  setAreaWall(newValues.areaWall);
  setButtonState(newValues.ButtonState);
};
