import { standardMeasures } from "../utils/standardMeasures";

export const verifyAreaDoorAndWindow = (
  setButtonState,
  areaWall,
  qtDoor = 0,
  qtWindow = 0,
  setTextExceededArea
) => {
  const totalAreaDoorAndWindow =
    qtDoor * standardMeasures.door.doorArea +
    qtWindow * standardMeasures.window.windowArea;

  if (totalAreaDoorAndWindow > areaWall * 0.5 && areaWall !== 0) {
    setButtonState(true);
    setTextExceededArea(
      "A área de portas e janelas não pode exceder 50% da área da parede"
    );
  } else {
    setTextExceededArea("");
    setButtonState(false);
  }
};
