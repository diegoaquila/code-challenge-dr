export const setLocalStorage = (key, data) => {
  try {
    const places = getLocalStorage(key);

    if (places) {
      places.push(data);
      data = places;
    }

    localStorage.setItem(key, JSON.stringify(data));
    return true;
  } catch (err) {
    return false;
  }
};

export const getLocalStorage = (key) => {
  try {
    const data = localStorage.getItem(key);
    if (data != null) {
      return JSON.parse(data);
    }
  } catch (err) {
    return false;
  }
};
