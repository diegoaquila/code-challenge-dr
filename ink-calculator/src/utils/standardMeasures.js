export const standardMeasures = {
  window: { width: 2, heigh: 1.2, windowArea: 2.4 },
  door: { width: 0.8, heigh: 1.9, doorArea: 1.52 },
  minHeightWallWithDoor: 2.2,
};
